package com.example.arief.todoapp.adapter;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.arief.todoapp.R;
import com.example.arief.todoapp.model.TodoList;
import com.example.arief.todoapp.ui.OnDeleteButtonDialog;
import com.example.arief.todoapp.viewmodel.MyViewModel;

import java.util.ArrayList;
import java.util.List;

public class RecViewListTaskAdapter extends RecyclerView.Adapter<RecViewListTaskAdapter.RecViewListTaskViewHolder> {



    private final String REC_VIEW_LIST_TASK_ADAPTER = RecViewListTaskAdapter.class.getSimpleName();


    private List<TodoList> todoListList = new ArrayList<>();


    public void updateData(List<TodoList> todoLists){
        this.todoListList = todoLists;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecViewListTaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_task,parent,false);
        return new RecViewListTaskViewHolder(v);
    }



    @Override
    public void onBindViewHolder(@NonNull RecViewListTaskViewHolder holder, int position) {
        TodoList todoList = todoListList.get(position);


        holder.setTodoId(todoList.getTodoId());
        holder.tTodoName.setText(todoList.getTodoName());
        holder.tTodoDesc.setText("Description \n"+todoList.getTodoDesc());
        holder.tTodoDeadline.setText("Deadline \n" + todoList.getTodoDeadline());
        if(todoList.getTodoStatus() == null) {
            todoList.setTodoStatus("Status OK");
        }
        holder.tTodoStatus.setText(todoList.getTodoStatus());

    }

    @Override
    public int getItemCount() {
        return todoListList.size();
    }

    class RecViewListTaskViewHolder extends RecyclerView.ViewHolder {

        TextView tTodoName, tTodoDesc, tTodoDeadline, tTodoStatus;
        ImageButton btnTodoDelete;
        String todoId;

        void setTodoId(String todoId) {
            this.todoId = todoId;
        }

        RecViewListTaskViewHolder(View itemView) {
            super(itemView);
            tTodoName = itemView.findViewById(R.id.t_todo_name);
            tTodoDesc = itemView.findViewById(R.id.t_todo_desc);
            tTodoDeadline = itemView.findViewById(R.id.t_todo_deadline);
            tTodoStatus = itemView.findViewById(R.id.t_todo_status);
            btnTodoDelete = itemView.findViewById(R.id.btn_todo_delete);
            btnTodoDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                    builder
                            .setTitle("Warning")
                            .setMessage("Are you sure to delete this task ?")
                            .setNegativeButton("Cancel",null)
                            .setPositiveButton("Confirm", new OnDeleteButtonDialog(todoId , v.getContext()))
                            .setCancelable(false)
                            .show();

                    Log.d(REC_VIEW_LIST_TASK_ADAPTER , v.getContext().getClass().getSimpleName() + " " +todoId+ " clicked...");
                }
            });
        }
    }
}

package com.example.arief.todoapp;

import android.arch.lifecycle.Observer;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.arief.todoapp.adapter.MyRecViewAdapter;
import com.example.arief.todoapp.api.TodoAPIService;
import com.example.arief.todoapp.model.ApiResponse;
import com.example.arief.todoapp.model.GenericResponse;
import com.example.arief.todoapp.model.User;
import com.example.arief.todoapp.ui.LoadingDialogFragment;
import com.example.arief.todoapp.utils.AppState;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recView;
    private MyRecViewAdapter recViewAdapter;
    private AppState appState;
    private LoadingDialogFragment loadingFragment = new LoadingDialogFragment();
    private TodoAPIService todoAPIService = new TodoAPIService();
    private final String MAINACTIVITY_TAG = "MAIN_ACTIVITY";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appState = new AppState(this);


        recView = findViewById(R.id.rec_view);
        recView.setHasFixedSize(true);


        StaggeredGridLayoutManager latMgr =
                new StaggeredGridLayoutManager(
                        2,
                        StaggeredGridLayoutManager.VERTICAL
                );
        recView.setLayoutManager(latMgr);


        recViewAdapter = new MyRecViewAdapter();
        recView.setAdapter(recViewAdapter);



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ab_menu , menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout_item:
                appState.clearState();
                return true;
            case R.id.login_item:
                AlertDialog.Builder dialog = new AlertDialog.Builder(this);
                View v = getLayoutInflater().inflate(R.layout.login_layout, null);

                final EditText eUsername = v.findViewById(R.id.e_username);
                final EditText ePassword = v.findViewById(R.id.e_password);



                dialog.setView(v)
                    .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                final String tUsername = eUsername.getText().toString();
                                String tPassword = ePassword.getText().toString();

                                if(tUsername.trim().isEmpty() || tPassword.trim().isEmpty()){
                                    Toast.makeText(MainActivity.this, "Invalid data", Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                User u = new User();
                                u.setUsername(tUsername);
                                u.setPassword(tPassword);

                                loadingFragment.setDialogTitle("Logging in user....");
                                loadingFragment.show(getSupportFragmentManager() , null);

                                System.err.println("isCancelable " +loadingFragment.isCancelable());


                                todoAPIService.authUser(u)
                                        .enqueue(new Callback<GenericResponse<Object>>() {
                                            @Override
                                            public void onResponse(Call<GenericResponse<Object>> call, Response<GenericResponse<Object>> response) {
                                                loadingFragment.dismiss();

                                                if(!response.isSuccessful()){
                                                    Toast.makeText(MainActivity.this, "Cannot login with this account", Toast.LENGTH_SHORT).show();
                                                    return;
                                                }


                                                Toast.makeText(MainActivity.this, "Login success", Toast.LENGTH_SHORT).show();
                                                appState.writeState("username",tUsername);
                                            }

                                            @Override
                                            public void onFailure(Call<GenericResponse<Object>> call, Throwable t) {
                                                loadingFragment.dismiss();
                                                Toast.makeText(MainActivity.this, "Error, cannot login to server", Toast.LENGTH_SHORT).show();
                                                Log.e(MAINACTIVITY_TAG , t.getMessage());
                                            }
                                        });

                            }catch (Exception ex){
                                Log.d(MAINACTIVITY_TAG , ex.getMessage());
                                Toast.makeText(MainActivity.this, "Invalid data", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }).show();




                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

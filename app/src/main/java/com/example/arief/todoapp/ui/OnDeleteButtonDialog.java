package com.example.arief.todoapp.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

import com.example.arief.todoapp.ListActivity;
import com.example.arief.todoapp.api.TodoAPIService;
import com.example.arief.todoapp.model.GenericResponse;
import com.example.arief.todoapp.utils.AppConst;
import com.example.arief.todoapp.utils.AppState;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OnDeleteButtonDialog implements DialogInterface.OnClickListener {


    private String currentTodoId;
    private Context c;
    private LoadingDialogFragment loadingFragment = new LoadingDialogFragment();
    private TodoAPIService apiService = new TodoAPIService();
    private AppState appState ;
    private final String ON_DELETE_DIALOG = OnDeleteButtonDialog.class.getSimpleName();




    public OnDeleteButtonDialog(String currentTodoId , Context c ) {
        this.currentTodoId = currentTodoId;
        this.c= c;
        appState = new AppState(c);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        final ListActivity ac = (ListActivity) c;
        loadingFragment.setDialogTitle("Processing data...");
        loadingFragment.show(((ListActivity) c).getSupportFragmentManager() , null);


        String currentUsername = appState.getValue("username");
        apiService
                .deleteTodo(currentTodoId , currentUsername)
                .enqueue(new Callback<GenericResponse<Object>>() {
                    @Override
                    public void onResponse(Call<GenericResponse<Object>> call, Response<GenericResponse<Object>> response) {
                        Log.d(ON_DELETE_DIALOG , "onResponse...");
                        loadingFragment.dismiss();

                        if(!response.isSuccessful()) {
                            Log.d(ON_DELETE_DIALOG , "response not success");
                            Toast.makeText(c, "Cannot delete this task", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        Log.d(ON_DELETE_DIALOG , "response success");
                        Toast.makeText(c, "Deleting...", Toast.LENGTH_SHORT).show();
                        ac.getActionLiveData().setValue(AppConst.ACTION_DELETE_TASK);
                    }

                    @Override
                    public void onFailure(Call<GenericResponse<Object>> call, Throwable t) {
                        loadingFragment.dismiss();
                        Toast.makeText(c, "Cannot delete this task", Toast.LENGTH_SHORT).show();
                        Log.e(ON_DELETE_DIALOG  , t.getMessage());
                    }
                });
    }
}

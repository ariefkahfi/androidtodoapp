package com.example.arief.todoapp;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.arief.todoapp.api.TodoAPIService;
import com.example.arief.todoapp.model.GenericResponse;
import com.example.arief.todoapp.model.TodoApiData;
import com.example.arief.todoapp.ui.DeadlineDatePickerDialogFragment;
import com.example.arief.todoapp.ui.LoadingDialogFragment;
import com.example.arief.todoapp.utils.AppState;
import com.example.arief.todoapp.viewmodel.MyViewModel;

import java.util.Calendar;

public class AddTaskActivity extends AppCompatActivity {


    private EditText eTaskName, eTaskDesription;
    private Button btnDeadline;

    private MyViewModel myViewModel;

    private final String TAG_ADDTASKACIVITY = "AddTaskActivity";

    private AppState appState;


    private TodoAPIService todoAPIService = new TodoAPIService();
    private LoadingDialogFragment loadingDialogFragment = new LoadingDialogFragment();



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_task);
        getSupportActionBar().setTitle("Add Task");
        appState = new AppState(this);
        eTaskName = findViewById(R.id.e_task_name);
        eTaskDesription = findViewById(R.id.e_task_desc);
        btnDeadline = findViewById(R.id.btn_deadline);
        myViewModel = ViewModelProviders.of(this).get(MyViewModel.class);


        todoAPIService
                .getNewTodoLiveData()
                .observe(this, new Observer<GenericResponse<Object>>() {
                    @Override
                    public void onChanged(@Nullable GenericResponse<Object> objectGenericResponse) {
                        loadingDialogFragment.dismiss();

                        if(objectGenericResponse == null){
                            Toast.makeText(AddTaskActivity.this, "Error from server", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        eTaskName.setText("");
                        eTaskDesription.setText("");
                        myViewModel.getCalendarMutableLiveData().setValue(null);
                        Toast.makeText(AddTaskActivity.this, "Add task success", Toast.LENGTH_SHORT).show();
                    }
                });

        myViewModel
                .getCalendarMutableLiveData()
                .observe(this, new Observer<Calendar>() {
                    @Override
                    public void onChanged(@Nullable Calendar calendar) {
                        if(calendar != null){
                            String sCalendar =
                                    calendar.get(Calendar.YEAR) + "-"
                                            + calendar.get(Calendar.MONTH) + "-"
                                            + calendar.get(Calendar.DAY_OF_MONTH);


                            btnDeadline.setText(sCalendar);
                            return;
                        }
                        btnDeadline.setText(getResources().getString(R.string.choose_date));
                    }
                });



    }





    public void onFormSubmit(View view) {
        try {
            String taskName = eTaskName.getText().toString().trim();
            String taskDesc = eTaskDesription.getText().toString().trim();
            String taskDeadline = btnDeadline.getText().toString().trim();

            if(!taskDeadline.contains("-") || taskName.isEmpty() || taskDesc.isEmpty()) {
                throw  new Exception("Invalid data");
            }

            TodoApiData todoApiData =
                    new TodoApiData.TodoApiDataBuilder()
                        .todoName(taskName)
                        .username(appState.getValue("username"))
                        .todoDescription(taskDesc)
                        .todoDeadline(taskDeadline)
                        .build();

            loadingDialogFragment.setDialogTitle("Adding new task...");
            loadingDialogFragment.show(getSupportFragmentManager() ,null);
            todoAPIService.newTodoLiveData(todoApiData);

//          sending todoApiData to API Server
            Log.d(TAG_ADDTASKACIVITY, todoApiData.toString());
        }catch (Exception ex) {
            Toast.makeText(this, "Invalid data", Toast.LENGTH_SHORT).show();
        }
    }

    public void onDeadline(View view) {
        DeadlineDatePickerDialogFragment ddf
                = new DeadlineDatePickerDialogFragment();
        ddf.show(getSupportFragmentManager() , null);
    }
}

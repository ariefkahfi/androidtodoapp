package com.example.arief.todoapp.model;

public class Card {
    private String textDesc;
    private int image;


    public Card(){}


    public Card(String textDesc, int image) {
        this.textDesc = textDesc;
        this.image = image;
    }

    public String getTextDesc() {
        return textDesc;
    }

    public void setTextDesc(String textDesc) {
        this.textDesc = textDesc;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}

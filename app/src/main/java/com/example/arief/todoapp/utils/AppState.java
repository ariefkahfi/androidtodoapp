package com.example.arief.todoapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class AppState {
    private SharedPreferences sharedPref ;



    public AppState (Context c) {
        sharedPref = c.getSharedPreferences(AppConst.APP_KEY , Context.MODE_PRIVATE);
    }


    public void writeState(String k , String v) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString( k , v);
        editor.apply();
    }

    public boolean getState(String k) {
        return sharedPref.getString(k , null) != null ;
    }

    public String getValue(String k){
        return sharedPref.getString(k , null);
    }

    public void clearState() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.apply();
    }

}

package com.example.arief.todoapp.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.View;

import com.example.arief.todoapp.R;

public class LoadingDialogFragment extends DialogFragment {



    public LoadingDialogFragment() {
        super.setCancelable(false);
    }


    private String aTitle = "";



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        return builder
                .setView(R.layout.dialog_loading)
                .setTitle(aTitle)
                .show();
    }

    public void setDialogTitle(String dialogTitle) {
        aTitle = dialogTitle;
    }

}

package com.example.arief.todoapp.model;

import com.google.gson.annotations.SerializedName;

public class TodoApiData {


    @SerializedName("username")
    private String username;

    @SerializedName("todo_name")
    private String todoName;

    @SerializedName("todo_desc")
    private String todoDescription;

    @SerializedName("todo_deadline")
    private String todoDeadline;


    private TodoApiData() {}


    public static class TodoApiDataBuilder {

        private TodoApiData todoApiData;

        public TodoApiDataBuilder() {
            todoApiData = new TodoApiData();
        }


        public TodoApiDataBuilder todoName(String name) {
            todoApiData.setTodoName(name);
            return this;
        }

        public TodoApiDataBuilder todoDescription(String desc){
            todoApiData.setTodoDescription(desc);
            return this;
        }

        public TodoApiDataBuilder todoDeadline(String deadline){
            todoApiData.setTodoDeadline(deadline);
            return this;
        }

        public TodoApiDataBuilder username(String username) {
            todoApiData.setUsername(username);
            return this;
        }

        public TodoApiData build() {
            return todoApiData;
        }
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTodoDescription() {
        return todoDescription;
    }

    public void setTodoDescription(String todoDescription) {
        this.todoDescription = todoDescription;
    }

    public String getTodoDeadline() {
        return todoDeadline;
    }

    public void setTodoDeadline(String todoDeadline) {
        this.todoDeadline = todoDeadline;
    }

    @Override
    public String toString() {
        return "TodoApiData{" +
                "todoName='" + todoName + '\'' +
                ", todoDescription='" + todoDescription + '\'' +
                ", todoDeadline='" + todoDeadline + '\'' +
                '}';
    }
}

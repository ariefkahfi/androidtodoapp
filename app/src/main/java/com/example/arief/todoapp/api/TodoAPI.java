package com.example.arief.todoapp.api;

import com.example.arief.todoapp.model.ApiResponse;
import com.example.arief.todoapp.model.GenericResponse;
import com.example.arief.todoapp.model.TodoApiData;
import com.example.arief.todoapp.model.TodoList;
import com.example.arief.todoapp.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface TodoAPI {

    @POST("user/auth")
    Call<GenericResponse<Object>> apiAuthUser(@Body User u);


    @DELETE("todo/{todoId}/{username}")
    Call<GenericResponse<Object>> apiDeleteTodo(@Path("todoId") String todoId , @Path("username") String username);

    @POST("todo")
    Call<GenericResponse<Object>> apiNewTodo(@Body TodoApiData todoApiData);

    @GET("user/{username}")
    Call<GenericResponse<List<TodoList>>> getTodosByUsername(@Path("username") String u);

}

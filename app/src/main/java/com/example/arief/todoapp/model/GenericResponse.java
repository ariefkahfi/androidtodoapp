package com.example.arief.todoapp.model;

import com.google.gson.annotations.SerializedName;

public class GenericResponse<T> {


    @SerializedName("code")
    private int code;

    @SerializedName("data")
    private T t;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    @Override
    public String toString() {
        return "GenericResponse{" +
                "code=" + code +
                ", t=" + t +
                '}';
    }
}

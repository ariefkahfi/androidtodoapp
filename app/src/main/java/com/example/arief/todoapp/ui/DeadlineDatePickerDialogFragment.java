package com.example.arief.todoapp.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import com.example.arief.todoapp.viewmodel.MyViewModel;

import java.util.Calendar;

public class DeadlineDatePickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {


    private MyViewModel myViewModel;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        myViewModel = ViewModelProviders.of(getActivity()).get(MyViewModel.class);
        Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR);
        int currentMonth = c.get(Calendar.MONTH);
        int currentDayOfMonth = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dDialog = new DatePickerDialog(getActivity(),this , currentYear,currentMonth , currentDayOfMonth);
        DatePicker dDatePicker  = dDialog.getDatePicker();

        Calendar cc = Calendar.getInstance();
        cc.add(Calendar.DAY_OF_MONTH , 2);
        long ccInMs = cc.getTimeInMillis();

        Calendar dd = Calendar.getInstance();
        dd.add(Calendar.DAY_OF_MONTH , 9);
        long ddInMs = dd.getTimeInMillis();


        dDatePicker.setMinDate(ccInMs);
        dDatePicker.setMaxDate(ddInMs);



        return dDialog;
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(year,month + 1,dayOfMonth);

        myViewModel
                .getCalendarMutableLiveData()
                .postValue(c);
    }
}

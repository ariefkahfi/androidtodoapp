package com.example.arief.todoapp.api;


import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.example.arief.todoapp.model.GenericResponse;
import com.example.arief.todoapp.model.TodoApiData;
import com.example.arief.todoapp.model.TodoList;
import com.example.arief.todoapp.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TodoAPIService {

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://192.168.1.101:8989/RestTodo/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    private TodoAPI todoAPI = retrofit.create(TodoAPI.class);

    private final String TODO_API_SERVICE_TAG = TodoAPIService.class.getName();



    public Call<GenericResponse<List<TodoList>>> getTodosByUsername(String username){
        return todoAPI.getTodosByUsername(username);
    }


    private MutableLiveData<GenericResponse<Object>> newTodoLiveData = new MutableLiveData<>();

    public MutableLiveData<GenericResponse<Object>> getNewTodoLiveData() {
        return newTodoLiveData;
    }


    public Call<GenericResponse<Object>> deleteTodo(String tId , String username) {
        return todoAPI.apiDeleteTodo(tId,username);
    }

    public void  newTodoLiveData(TodoApiData todoApiData){

        todoAPI.apiNewTodo(todoApiData)
                .enqueue(new Callback<GenericResponse<Object>>() {
                    @Override
                    public void onResponse(Call<GenericResponse<Object>> call, Response<GenericResponse<Object>> response) {
                        Log.e(TODO_API_SERVICE_TAG , response.message());

                        if(response.isSuccessful()){
                            newTodoLiveData.setValue(response.body());
                            return;
                        }
                        newTodoLiveData.setValue(null);
                    }

                    @Override
                    public void onFailure(Call<GenericResponse<Object>> call, Throwable t) {
                        Log.e(TODO_API_SERVICE_TAG , "onFailure",t);
                        newTodoLiveData.setValue(null);
                    }
                });
    }


    public Call<GenericResponse<Object>> newTodo(TodoApiData todoApiData){
        return todoAPI.apiNewTodo(todoApiData);
    }


    public Call<GenericResponse<Object>> authUser(User u){
        return todoAPI.apiAuthUser(u);
    }


}

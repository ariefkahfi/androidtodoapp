package com.example.arief.todoapp.utils;

public interface AppConst {
    public final static String APP_KEY = "com.example.myapp.todoapp_key";
    public final static String ACTION_DELETE_TASK = "DELETE_TASK";
}

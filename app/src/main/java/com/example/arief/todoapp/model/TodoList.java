package com.example.arief.todoapp.model;

import com.google.gson.annotations.SerializedName;

public class TodoList {

    @SerializedName("todo_id")
    private String todoId;

    @SerializedName("todo_name")
    private String todoName;

    @SerializedName("todo_desc")
    private String todoDesc;

    @SerializedName("todo_deadline")
    private String todoDeadline;

    @SerializedName("todo_status")
    private String todoStatus;


    public String getTodoId() {
        return todoId;
    }

    public void setTodoId(String todoId) {
        this.todoId = todoId;
    }

    public String getTodoName() {
        return todoName;
    }

    public void setTodoName(String todoName) {
        this.todoName = todoName;
    }

    public String getTodoDesc() {
        return todoDesc;
    }

    public void setTodoDesc(String todoDesc) {
        this.todoDesc = todoDesc;
    }

    public String getTodoDeadline() {
        return todoDeadline;
    }

    public void setTodoDeadline(String todoDeadline) {
        this.todoDeadline = todoDeadline;
    }

    public String getTodoStatus() {
        return todoStatus;
    }

    public void setTodoStatus(String status) {
        this.todoStatus = status;
    }

    @Override
    public String toString() {
        return "TodoList{" +
                "todoId='" + todoId + '\'' +
                ", todoName='" + todoName + '\'' +
                ", todoDesc='" + todoDesc + '\'' +
                ", todoDeadline='" + todoDeadline + '\'' +
                ", todoStatus='" + todoStatus + '\'' +
                '}';
    }
}

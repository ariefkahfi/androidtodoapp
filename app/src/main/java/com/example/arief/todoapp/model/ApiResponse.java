package com.example.arief.todoapp.model;

import com.google.gson.annotations.SerializedName;

public class ApiResponse {


    @SerializedName("code")
    private int code;

    @SerializedName("data")
    private Object data;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}

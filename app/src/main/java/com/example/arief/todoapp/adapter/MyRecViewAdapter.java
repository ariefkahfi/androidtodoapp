package com.example.arief.todoapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.arief.todoapp.AddTaskActivity;
import com.example.arief.todoapp.FeedbackActivity;
import com.example.arief.todoapp.ListActivity;
import com.example.arief.todoapp.MyProfileActivity;
import com.example.arief.todoapp.R;
import com.example.arief.todoapp.model.Card;
import com.example.arief.todoapp.utils.AppState;

public class MyRecViewAdapter extends RecyclerView.Adapter<MyRecViewAdapter.MyRecViewHolder> {

    private final Card[] CARDS = new Card[]{
            new Card("Add task",R.drawable.ic_add_black_24dp),
            new Card("List task",R.drawable.ic_list_black_24dp),
            new Card("My profile",R.drawable.ic_person_white_24dp),
            new Card("Feedback",R.drawable.ic_feedback_black_24dp)
    };


    @NonNull
    @Override
    public MyRecViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout,parent,false);
        return new MyRecViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyRecViewHolder holder, int position) {
        Card currentCard = CARDS[position];

        holder.cardText.setText(currentCard.getTextDesc());
        holder.cardImage.setImageResource(currentCard.getImage());
    }

    @Override
    public int getItemCount() {
        return CARDS.length;
    }

    class MyRecViewHolder extends  RecyclerView.ViewHolder {


        private ImageView cardImage;
        private TextView cardText;

        MyRecViewHolder(final View itemView) {
            super(itemView);
            final AppState appState = new AppState(itemView.getContext());

            cardImage = itemView.findViewById(R.id.card_image);
            cardText = itemView.findViewById(R.id.card_text);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Card currentCard = CARDS[getAdapterPosition()];

                    Log.d("MyRecViewAdapter", currentCard.getTextDesc());

                    Intent i = null;

                    boolean currentSession = appState.getState("username");


                    if (currentCard.getTextDesc().equals("Add task")) {
                        if(!currentSession){
                            Toast.makeText(itemView.getContext(), "Login first", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        i = new Intent(itemView.getContext(), AddTaskActivity.class);
                    } else if (currentCard.getTextDesc().equals("My profile")) {
                        if(!currentSession){
                            Toast.makeText(itemView.getContext(), "Login first", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        i = new Intent(itemView.getContext() , MyProfileActivity.class);
                    } else if (currentCard.getTextDesc().equals("Feedback")) {
                        i = new Intent(itemView.getContext() , FeedbackActivity.class);
                    } else if (currentCard.getTextDesc().equals("List task")) {
                        if(!currentSession){
                            Toast.makeText(itemView.getContext(), "Login first", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        i = new Intent(itemView.getContext() , ListActivity.class);
                    }

                    (itemView.getContext()).startActivity(i);
                }
            });
        }
    }

}

package com.example.arief.todoapp;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.arief.todoapp.adapter.RecViewListTaskAdapter;
import com.example.arief.todoapp.api.TodoAPIService;
import com.example.arief.todoapp.model.GenericResponse;
import com.example.arief.todoapp.model.TodoList;
import com.example.arief.todoapp.ui.LoadingDialogFragment;
import com.example.arief.todoapp.utils.AppConst;
import com.example.arief.todoapp.utils.AppState;
import com.example.arief.todoapp.viewmodel.MyViewModel;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListActivity extends AppCompatActivity {



    private RecyclerView recViewListTask;
    private RecViewListTaskAdapter listTaskAdapter;
    private TodoAPIService todoAPIService = new TodoAPIService();
    private MutableLiveData<String> actionLiveData = new MutableLiveData<>();

    private LoadingDialogFragment loadingFragment = new LoadingDialogFragment();


    private AppState appState;


    private final String LIST_ACTIVITY_TAG = "ListActivity";



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_task);
        appState = new AppState(this);
        getSupportActionBar().setTitle("List Task");
        recViewListTask = findViewById(R.id.rec_list_task);


        LinearLayoutManager latMgr = new LinearLayoutManager(this);
        recViewListTask.setHasFixedSize(true);
        recViewListTask.setLayoutManager(latMgr);


        listTaskAdapter = new RecViewListTaskAdapter();

        recViewListTask.setAdapter(listTaskAdapter);



        getActionLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if(s.equals(AppConst.ACTION_DELETE_TASK)) {
                    fetchTaskList();
                }
            }
        });


        fetchTaskList();

    }



    public MutableLiveData<String> getActionLiveData() {
        return actionLiveData;
    }

    private void fetchTaskList() {
        loadingFragment.setDialogTitle("Fetching... your task list from server...");
        loadingFragment.show(getSupportFragmentManager() , null);

        todoAPIService
                .getTodosByUsername(appState.getValue("username"))
                .enqueue(new Callback<GenericResponse<List<TodoList>>>() {

                    @Override
                    public void onResponse(Call<GenericResponse<List<TodoList>>> call, Response<GenericResponse<List<TodoList>>> response) {
                        loadingFragment.dismiss();

                        if(!response.isSuccessful()){
                            Toast.makeText(ListActivity.this, "Failed to load data...", Toast.LENGTH_SHORT).show();
                            try {
                                String s =response.errorBody().string();
                                System.err.println(s);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            return;
                        }

                        Toast.makeText(ListActivity.this, "successfully get your data...", Toast.LENGTH_SHORT).show();
                        GenericResponse<List<TodoList>> body = response.body();

                        if(body != null && body.getT() != null) {
                            listTaskAdapter.updateData(body.getT());
                        }

                    }

                    @Override
                    public void onFailure(Call<GenericResponse<List<TodoList>>> call, Throwable t) {
                        loadingFragment.dismiss();
                        Toast.makeText(ListActivity.this, "Failed to load data...", Toast.LENGTH_SHORT).show();
                        Log.e(LIST_ACTIVITY_TAG , t.getMessage());
                    }
                });
    }


}

package com.example.arief.todoapp.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import java.util.Calendar;

public class MyViewModel extends AndroidViewModel {
    private MutableLiveData<Calendar> calendarMutableLiveData = new MutableLiveData<>();


    public MyViewModel(@NonNull Application application) {
        super(application);
    }



    public MutableLiveData<Calendar> getCalendarMutableLiveData() {
        return calendarMutableLiveData;
    }

}
